﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; }}

    public GameObject uiCanvas;
    public GameObject retryButton;
    public GameObject nextButton;
    public GameObject exitButton;
    public GameObject loseUI;
    public GameObject pauseUI;
    public Text score;
    public Text lose;


    private bool isPaused;

    public enum LoseCondition
    {
        RunOver,
        Drowned,
        Victory
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        isPaused = false;
        DisableUI();
        RefreshScore();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                UnPauseGame();
            }
            else
            {
                PauseGame();
            }
            isPaused = !isPaused;
        }
    }

    private void UnPauseGame()
    {
        Time.timeScale = 1.0f;
        pauseUI.GetComponent<Animator>().SetTrigger("FadeFromBlack");
    }

    private void PauseGame()
    {
        Time.timeScale = 0.0f;
        pauseUI.GetComponent<Animator>().SetTrigger("FadeToBlack");
    }

    public void DisableUI()
    {
        retryButton.SetActive(false);
        exitButton.SetActive(false);
        nextButton.SetActive(false);
        loseUI.SetActive(false);
        Player.Instance.canMove = true;
    }

    public void EnableUI(LoseCondition loseCondition)
    {
        
        exitButton.SetActive(true);
        loseUI.SetActive(true);
        switch (loseCondition)
        {
            case LoseCondition.RunOver:
                Player.Instance.KillPlayer();
                Player.Instance.audioSourceCar.Play();
                lose.text = "You were run over by a car";
                retryButton.SetActive(true);
                break;
            case LoseCondition.Drowned:
                Player.Instance.KillPlayer();
                Player.Instance.audioSourceBubbles.Play();
                lose.text = "You drowned in a river";
                retryButton.SetActive(true);
                break;
            case LoseCondition.Victory:
                lose.text = "You were victorious";
                if (SceneManager.GetActiveScene().name.Contains("Infinite"))
                {
                    retryButton.SetActive(true);
                    retryButton.GetComponentInChildren<Text>().text = "Replay";
                }
                else
                {
                    nextButton.SetActive(true);
                }
                break;
            default:
                break;
        }
    }

    public void RefreshScore()
    {
        score.text = Player.Instance.scoreValue.ToString();
    }
}
