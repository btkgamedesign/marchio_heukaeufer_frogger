﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    private static CameraTarget _instance;
    public static CameraTarget Instance { get { return _instance; } }
    public bool follow;
    public Transform player;
    Vector3 shift;

    private void Awake()
    {
        _instance = this;
        shift = transform.position - player.position;
        follow = true;
    }

    private void Update()
    {
        if (follow)
        {
            transform.position = player.position + shift;
        }
    }
}
