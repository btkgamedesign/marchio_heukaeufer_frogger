﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private static Player _instance;
    public static Player Instance
    {
        get { return _instance; }
    }
    public delegate void MoveAction();
    public static event MoveAction OnMove;

    public AudioSource audioSourceBubbles;
    public AudioSource audioSourceCar;
    public AudioSource[] jumpAudioSources;

    public GameObject smashedCubePrefab;
    public Transform northWall;
    public Transform southWall;
    public Transform westWall;
    public Transform eastWall;

    public int scoreValue;
    public bool isPlayerAlive;
    public bool canMove;

    private bool isMoving;
    private int audioSourceCounter = 0;

    private Vector3 forward = new Vector3(1, 0, 0);
    private Vector3 left = new Vector3(0, 0, 1);

    private void OnEnable()
    {
        OnMove += PlayMoveSound;
    }

    private void OnDisable()
    {
        OnMove -= PlayMoveSound;
    }

    private void Awake()
    {
        _instance = this;
        scoreValue = 0;
        canMove = true;
        isPlayerAlive = true;
        WiggleToEnsureCollisionDetection();
    }

    private void Update()
    {
        Vector3 target = transform.position;
        if (canMove)
        {
            if (!isMoving)
            {
                if (Input.GetKeyUp(KeyCode.UpArrow))
                {
                    target += forward;
                    target.Set(Mathf.Min(target.x, northWall.position.x - 1), target.y, target.z);
                    if (OnMove != null)
                        OnMove();
                }
                else if (Input.GetKeyUp(KeyCode.DownArrow))
                {
                    target -= forward;
                    target.Set(Mathf.Max(target.x, southWall.position.x + 1), target.y, target.z);
                    if (OnMove != null)
                        OnMove();
                }
                else if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    target += left;
                    target.Set(target.x, target.y, Mathf.Min(target.z, westWall.position.z - 1));
                    if (OnMove != null)
                        OnMove();
                }
                else if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    target -= left;
                    target.Set(target.x, target.y, Mathf.Max(target.z, eastWall.position.z + 1));
                    if (OnMove != null)
                        OnMove();
                }
            }

            if (Vector3.SqrMagnitude(transform.position - target) > 0.01f)
            {
                isMoving = true;
                transform.DOMove(target, .2f).OnComplete(() => isMoving = false);
            }
        }

        if (isPlayerAlive)
        {
            if (transform.position.y <= -0.2f)
            {
                if (canMove)
                {
                    UIManager.Instance.EnableUI(UIManager.LoseCondition.Drowned);
                    canMove = false;
                }
            }
        }
    }

    private void PlayMoveSound()
    {
        if (audioSourceCounter == jumpAudioSources.Length)
            audioSourceCounter = 0;
        jumpAudioSources[audioSourceCounter].pitch = Random.Range(0.8f, 1.2f);
        jumpAudioSources[audioSourceCounter].volume = Random.Range(0.3f, 0.5f);
        jumpAudioSources[audioSourceCounter].Play();
        audioSourceCounter++;
    }

    private void WiggleToEnsureCollisionDetection()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(transform.DOScale(new Vector3(0.85f, 0.85f, 0.85f), 1f));
        sequence.SetLoops(-1, LoopType.Yoyo);
    }

    public void KillPlayer()
    {
        isPlayerAlive = false;
        Instantiate(smashedCubePrefab, transform.position, Quaternion.identity);
        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.GetComponent<Renderer>().enabled = false;
        CameraTarget.Instance.follow = false;
    }
}
