﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignWithPlayer : MonoBehaviour
{
    public Transform minX;
    public Transform maxX;
    private Player player;

    private AudioSource audioSource;

    private void Awake()
    {
        player = Player.Instance;
    }

    private void OnEnable()
    {
        Player.OnMove += UpdateAudioLocation;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnDisable()
    {
        Player.OnMove -= UpdateAudioLocation;
    }

    private void UpdateAudioLocation()
    {
        transform.position = new Vector3(Mathf.Clamp(Player.Instance.transform.position.x, minX.position.x, maxX.position.x), transform.position.y, Player.Instance.transform.position.z);
        if (Vector3.Distance(Player.Instance.transform.position, transform.position) < 2f)
        {
            audioSource.spatialBlend = 0f;
        }
        else
        {
            audioSource.spatialBlend = 1f;
        }
    }
}
