﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    //This scripts is supposed to scale the environment down when the player gets closer in order
    //to allow an easier viewing experience and thus better gameplay

    public Mesh[] treeMeshes;
    public Material[] treeMaterials;
    public float maxDistance = 1f;
    public Transform parent;

    private float distanceToPlayer;
    private Vector3 startScale;

    private IEnumerator lerpScale;

    private void Awake()
    {
        startScale = parent.transform.localScale;
        gameObject.GetComponent<MeshFilter>().mesh = treeMeshes[Random.Range(0, treeMeshes.Length)];
        GetComponent<Renderer>().material = treeMaterials[Random.Range(0, treeMaterials.Length)];
    }

    public void UpdateIndividualTreeScale()
    {
        distanceToPlayer = Vector3.Distance(transform.position, Player.Instance.transform.position);
        if (distanceToPlayer < maxDistance)
        {
            if (lerpScale != null)
                StopCoroutine(lerpScale);
            float scaleFactor = GetScaleFactor(distanceToPlayer);
            lerpScale = LerpScale(new Vector3(scaleFactor, scaleFactor, scaleFactor));
            StartCoroutine(lerpScale);
        }
        else
        {
            if (lerpScale != null)
            {
                StopCoroutine(lerpScale);
                lerpScale = LerpScale(startScale);
                StartCoroutine(lerpScale);
            }
        }
    }

    private float GetScaleFactor(float value)
    {
        float invertedValue = Mathf.Sqrt(value)/3;
        return invertedValue;
    }

    IEnumerator LerpScale(Vector3 targetScale)
    {
        int scalingFramesLeft = 10;
        while (scalingFramesLeft > 0)
        {
            parent.transform.localScale = Vector3.Lerp(parent.transform.localScale, targetScale, Time.deltaTime * 2);
            yield return scalingFramesLeft--;
        }
    }
}
