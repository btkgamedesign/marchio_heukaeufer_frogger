﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnvironmentScaleManager : MonoBehaviour
{
    private static EnvironmentScaleManager _instance;
    public static EnvironmentScaleManager Instance { get { return _instance; }}

    public List<Tree> trees;

    int layerMaskTree = 1 << 9; // Layermask 9 && bitshifting to the left 10 bits, actually fascinating

    private void OnEnable()
    {
        Player.OnMove += UpdateTreeScale;
        SceneManager.sceneLoaded += Initialize;
    }

    private void OnDisable()
    {
        Player.OnMove -= UpdateTreeScale;
        SceneManager.sceneLoaded -= Initialize;
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Initialize(Scene scene, LoadSceneMode mode)
    {
        GetTreesInProximity();
        UpdateTreeScale();
    }

    public void UpdateTreeScale()
    {
        GetTreesInProximity();
        StartCoroutine(UpdateReplacement());
    }

    private void GetTreesInProximity()
    {
        trees = new List<Tree>();

        Collider[] treeColliders = Physics.OverlapSphere(transform.position, 8f, layerMaskTree);
        foreach (Collider treeCollider in treeColliders)
        {
            trees.Add(treeCollider.gameObject.GetComponent<Tree>());
        }
    }

    IEnumerator UpdateReplacement()
    {
        for (int i = 0; i < 60; i++)
        {
            foreach (Tree tree in trees)
            {
                tree.UpdateIndividualTreeScale();
            }
            yield return null;
        }
    }
}
