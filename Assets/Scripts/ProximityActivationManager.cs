﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProximityActivationManager : MonoBehaviour
{
    private static ProximityActivationManager _instance;
    public static ProximityActivationManager Instance { get { return _instance; } }
    private int layerMaskMustKeep;
    public List<GameObject> gameObjects;

    private void OnEnable()
    {
        Player.OnMove += ActivateProximity;
        //SceneManager.sceneLoaded += GatherGameObjects;
    }

    private void OnDisable()
    {
        Player.OnMove -= ActivateProximity;
        //SceneManager.sceneLoaded -= GatherGameObjects;
    }

    private void Awake()
    {
        _instance = this;
        layerMaskMustKeep = 1 << 10;
        layerMaskMustKeep = ~layerMaskMustKeep;
    }

    public void GatherGameObjects()
    {
        gameObjects = new List<GameObject>();
        foreach (Collider collider in Physics.OverlapSphere(transform.position, Mathf.Infinity, layerMaskMustKeep))
        {
            if (!gameObjects.Contains(collider.gameObject))
            {
                gameObjects.Add(collider.gameObject);
            }
        }

        ActivateProximity();
    }

    private void ActivateProximity()
    {       
        foreach (GameObject gameObject in gameObjects)
        {
            if (Vector3.Distance(gameObject.transform.position, transform.position) > 20f)
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }
        }
    }

    public void RemoveFromList(GameObject gameObject)
    {
        gameObjects.Remove(gameObject);
    }
}
