﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    private void Start()
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.angularVelocity = new Vector3(0, Random.Range(1f,2f), 0);

        Sequence floatUpAndDownSequence = DOTween.Sequence();
        floatUpAndDownSequence.Append(transform.DOMoveY(1, Random.Range(1.5f, 2.5f)));
        floatUpAndDownSequence.SetLoops(-1, LoopType.Yoyo);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            GetComponentInChildren<Renderer>().enabled = false;
            foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
            {
                spriteRenderer.enabled = false;
            }
            Player.Instance.scoreValue++;
            UIManager.Instance.RefreshScore();
            AudioSource audioSource = gameObject.GetComponent<AudioSource>();
            audioSource.volume = Random.Range(0.25f, 1f);
            audioSource.pitch = Random.Range(0.75f, 1.25f);
            audioSource.Play();
            ProximityActivationManager.Instance.RemoveFromList(this.gameObject);
            Destroy(this.gameObject, audioSource.clip.length);
        }
    }
}
