﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forest : MonoBehaviour
{
    public Transform[] treeSpawnPoints;
    public GameObject treePrefab;
    public GameObject[] plantPrefab;

    private void Awake()
    {
        Transform trees = GameObject.Find("Trees").transform;
        Transform plants = GameObject.Find("Plants").transform;
        foreach (Transform treeSpawnPoint in treeSpawnPoints)
        {
            if (Random.value < 0.75f)
            {
                Instantiate(treePrefab, treeSpawnPoint.position, Quaternion.identity, trees);
            }
            else if (Random.value < 0.5f)
            {
                Instantiate(plantPrefab[Random.Range(0, plantPrefab.Length)], treeSpawnPoint.position, Quaternion.identity, plants);
            }
        }
    }
}
