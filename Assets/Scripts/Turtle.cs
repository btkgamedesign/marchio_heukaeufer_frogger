﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Turtle : MonoBehaviour
{
    public float yTarget;

    private void Awake()
    {
        StartCoroutine(RandomSubMerge(Random.Range(0f, 2f)));
    }

    IEnumerator RandomSubMerge(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        // Sequence is a class of DOTween
        Sequence submergingSequence = DOTween.Sequence();
        submergingSequence.Append(transform.DOMoveY(yTarget, 2));
        submergingSequence.SetLoops(-1, LoopType.Yoyo);
        submergingSequence.PrependInterval(1);
    }
}
