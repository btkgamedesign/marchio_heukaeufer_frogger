﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    public Transform[] grassSpawnPoints;
    public GameObject[] grassPrefabs;

    private void Awake()
    {
        foreach (Transform grassSpawnPoint in grassSpawnPoints)
        {
            if (Random.value < 0.5f)
            {
                Instantiate(grassPrefabs[Random.Range(0, grassPrefabs.Length)], grassSpawnPoint.position, Quaternion.identity, GameObject.Find("Plants").transform);
            }
        }
    }
}
