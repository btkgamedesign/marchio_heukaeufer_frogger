﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Car : MonoBehaviour
{
	public bool isMovingLeft;
	public float speedFactor;
	public Light[] lights;
	private Vector3 movement;
	private float lastWarp;
	private bool canMove;
	private AudioSource audioSource;

    public void Initialize()
    {
		lastWarp = Time.time - 3;
		canMove = true;
		ChangeLightState(false);

		if (isMovingLeft)
		{
			movement = new Vector3(0, 0, 1);
			transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else
		{
			movement = new Vector3(0, 0, -1);
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.volume = Random.Range(0.25f, 0.75f);
		if (Random.value < 0.5f)
		{
			audioSource.pitch = Random.Range(0.95f, 1.05f);
		}
	}

	void Update()
	{
		if (canMove)
        {
			Vector3 move = movement * speedFactor * Time.deltaTime;
			transform.position += move;
        }
	}

	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<Wall>() != null)
        {
			float now = Time.time;
			if (now > lastWarp + 3)
            {
				lastWarp = now;
				transform.position = transform.position + new Vector3(0, 0, -other.transform.position.z * 2.2f);
				StartCoroutine(WarnPlayer());
            }
        }
	}

	private IEnumerator WarnPlayer()
    {
		canMove = false;
		bool isLightsEnabled = false;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 2; j++)
            {
				ChangeLightState(isLightsEnabled);
				isLightsEnabled = !isLightsEnabled;
				yield return new WaitForSeconds(0.1f);
            }
        }
		ChangeLightState(false);
		canMove = true;
    }

	private void ChangeLightState(bool state)
    {
        foreach (Light light in lights)
        {
			light.enabled = state;
        }
        if (state)
        {
			audioSource.Play();
        }
    }

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.GetComponent<Player>() != null)
		{
			UIManager.Instance.EnableUI(UIManager.LoseCondition.RunOver);
		}
	}
}
