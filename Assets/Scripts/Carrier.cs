﻿using UnityEngine;

public class Carrier : MonoBehaviour
{
    public bool isMovingLeft;
    public float speed = 1f;
    Transform player;
    Vector3 movement;


    void Start()
    {
        if (isMovingLeft)
        {
            movement = new Vector3(0, 0, 1 * speed);
        }
        else
        {
            movement = new Vector3(0, 0, -1 * speed);
        }
    }

    void Update()
    {
        Vector3 move = movement * Time.deltaTime;
        transform.position += move;
        if (player != null)
        {
            player.position += move;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            player = collision.transform;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            player = null;
        }
    }

    float lastWarped;
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Wall>() != null)
        {
            float now = Time.time;
            if (now > lastWarped + 2)
            {
                lastWarped = now;
                transform.position = transform.position + new Vector3(0, 0, -other.transform.position.z * 2);
            }
        }
    }
}
