﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashedCube : MonoBehaviour
{
    private void Awake()
    {
        foreach (Rigidbody rigidbody in gameObject.GetComponentsInChildren<Rigidbody>())
        {
            rigidbody.AddForce(new Vector3(Random.Range(3f, 5f), Random.Range(3f, 5f), Random.Range(3f, 5f)), ForceMode.Impulse);
        }
    }
}
