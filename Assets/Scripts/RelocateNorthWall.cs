﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelocateNorthWall : MonoBehaviour
{
    public void Initialize()
    {
        transform.position = new Vector3(FindObjectOfType<Homes>().transform.position.x + 1, 0, 0);
    }
}
