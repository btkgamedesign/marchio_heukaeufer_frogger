﻿using UnityEngine;

[System.Serializable]
public class Segment
{
    public GameObject segmentPrefab;
    public float segmentWidth;

}
