﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homes : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>())
        {
            UIManager.Instance.EnableUI(UIManager.LoseCondition.Victory);
            gameObject.GetComponent<AudioSource>().Play();
            Player.Instance.canMove = false;
        }
    }
}
