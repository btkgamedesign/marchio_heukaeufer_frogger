﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public void Awake()
    {
        Time.timeScale = 0.0f;
        if (TutorialManager.Instance.tutorialSeenCounter <= 0)
        {
            GetComponent<Animator>().SetTrigger("ShowTutorial");
            TutorialManager.Instance.tutorialSeenCounter++;
            return;
        }

        Time.timeScale = 1.0f;
    }

    public void StopGame()
    {
        Time.timeScale = 0.0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1.0f;
    }
}
