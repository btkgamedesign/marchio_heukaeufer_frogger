﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSetUpManager : MonoBehaviour
{
    [Header("UI")]
    public GameObject canvas;
    public Text feedback;
    public Text progress;
    public Slider slider;
    [Header("Segments")]
    public int amountOfSegments;
    public Segment startArea;
    public Segment homesArea;
    public List<Segment> segments;

    private float offset;

    private void Awake()
    {
        StartCoroutine(FeedbackAwake());
    }

    IEnumerator FeedbackAwake()
    {
        Time.timeScale = 0f;
        canvas.SetActive(true);
        Instantiate(startArea.segmentPrefab, new Vector3(-5.5f, 0, 0), Quaternion.identity);
        offset = startArea.segmentWidth - 5.5f;
        slider.maxValue = amountOfSegments;
        for (int i = 0; i < amountOfSegments; i++)
        {
            int index = Random.Range(0, segments.Count);
            Vector3 segmentPosition = new Vector3(offset, 0, 0);
            GameObject tmp = Instantiate(segments[index].segmentPrefab, segmentPosition, Quaternion.identity);
            offset += segments[index].segmentWidth;
            feedback.text = tmp.name;
            progress.text = ((i + 1) * 100) / amountOfSegments + "%";
            yield return slider.value = i;
        }

        Instantiate(homesArea.segmentPrefab, new Vector3(offset, 0, 0), Quaternion.identity);
        GameObject.Find("northWall").GetComponent<RelocateNorthWall>().Initialize();
        feedback.text = "Ensuring Performance";
        for (int i = 0; i < 25; i++)
        {
            yield return null;
        }
        canvas.SetActive(false);
        ProximityActivationManager.Instance.GatherGameObjects();
        Time.timeScale = 1f;
    }
}
