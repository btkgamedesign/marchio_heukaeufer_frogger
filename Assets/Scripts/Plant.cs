﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    public Material[] plantMaterials;

    private IEnumerator lerpScale;

    private void Awake()
    {
        GetComponentInChildren<Renderer>().material = plantMaterials[Random.Range(0, plantMaterials.Length)];
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            if (lerpScale != null)
                StopCoroutine(lerpScale);
            lerpScale = LerpScale(Vector3.zero, 10f);
            StartCoroutine(lerpScale);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            if (lerpScale != null)
                StopCoroutine(lerpScale);
            lerpScale = LerpScale(Vector3.one, 2f);
            StartCoroutine(lerpScale);
        }
    }

    IEnumerator LerpScale(Vector3 scaleVector, float speed)
    {
        int framesLeft = 300;
        Transform geoTransform = transform.GetChild(0).transform;

        while (framesLeft > 0)
        {
            geoTransform.localScale = Vector3.Lerp(geoTransform.localScale, scaleVector, Time.deltaTime * speed);
            yield return framesLeft--;
        }
    }
}
