﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private void OnEnable()
    {
        Player.OnMove += StickToPlayer;
    }

    private void OnDisable()
    {
        Player.OnMove -= StickToPlayer;
    }

    private void StickToPlayer()
    {
        transform.position = new Vector3(Player.Instance.transform.position.x, transform.position.y, transform.position.z);
    }
}
