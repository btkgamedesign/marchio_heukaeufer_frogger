﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public Transform[] carSpawnPoints;
    public Transform[] coinSpawnPoints;
    public GameObject[] carPrefabs;
    public GameObject coin;
    private bool isLeftLane;
    private float speed;

    private Transform carsParent;
    private Transform coinsParent;

    private void Awake()
    {
        carsParent = GameObject.Find("Cars").transform;
        coinsParent = GameObject.Find("Coins").transform;

        if (Random.value < 0.5f)
            isLeftLane = true;
        else
            isLeftLane = false;

        speed = Random.Range(0.8f, 1.2f);

        foreach (Transform carSpawnPoint in carSpawnPoints)
        {
            if (Random.value < 0.6f)
            {
                GameObject carObj = Instantiate(carPrefabs[Random.Range(0, carPrefabs.Length)], new Vector3(carSpawnPoint.position.x, carSpawnPoint.position.y + 0.5f, carSpawnPoint.position.z), Quaternion.identity, carsParent);
                Car car = carObj.GetComponent<Car>();
                car.isMovingLeft = isLeftLane;
                car.speedFactor = speed;
                car.Initialize();
            }
        }

        foreach (Transform coinSpawnPoint in coinSpawnPoints)
        {
            if (Random.value < 0.15f)
            {
                Instantiate(coin, new Vector3(coinSpawnPoint.position.x, coinSpawnPoint.position.y + 0.5f, coinSpawnPoint.position.z), Quaternion.identity, coinsParent);
            }
        }
    }
}
