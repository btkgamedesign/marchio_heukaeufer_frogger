﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    private static TutorialManager _instance;

    public int tutorialSeenCounter;

    public static TutorialManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null)
        {
            Debug.Log("TutorialManager:: Duplicate found, deleting this duplicate");
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
            tutorialSeenCounter = 0;
        }
    }
}
