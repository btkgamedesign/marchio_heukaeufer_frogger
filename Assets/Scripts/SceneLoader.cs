﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public GameObject backGround;
    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;

    private void Awake()
    {
        loadingScreen.SetActive(false);
        backGround.SetActive(false);
    }

    public void LoadScene(int index)
    {
        UIManager.Instance.DisableUI();
        StartCoroutine(LoadAsynchronously(index));
        //SceneManager.LoadScene(index);
    }

    public void LoadNextScene()
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex + 1));
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ReloadScene()
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex));
        //LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);
        backGround.SetActive(true);

        while (!asyncOperation.isDone)
        {
            float progress = Mathf.Clamp01(asyncOperation.progress / 0.9f);

            slider.value = progress;
            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
    UnityEditor.EditorApplication.isPlaying = false;
#else
    Application.Quit();
#endif
    }
}
